import React from 'react'
import "animate.css"
import header from "../../img/header.png"
export default function FeaturedProducts() {

  return (
    <div>
        <h2 className='text-center py-5'>Featured Products</h2>
        <div className= "row  px-5 animate__slow animate__animated animate__bounceInUp" >
            <div className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cHJvZHVjdHxlbnwwfHwwfHw%3D&w=1000&q=80" alt="product image" />
                <p className='fs-4'>Cherry Moon Coffee Table - <span>$1859</span></p>
                <span>Learn more</span>
            </div>
            <div className="animate__bounceInUp col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://live.staticflickr.com/65535/51061380037_1b373ecee8_b.jpg" alt="product image" />
                <p className='fs-4'>Club Deep Seating Settee - <span>$1500</span></p>
                <span>Learn more</span>
            </div>
            <div  className="animate__bounceInUp col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://media.istockphoto.com/photos/audiobooks-concept-picture-id1285965933?b=1&k=20&m=1285965933&s=170667a&w=0&h=J7zoMTM4nsooAQRSU8MrxziEjW6goMjof4sji08Gp3c=" alt="product image" />
                <p className='fs-4'>Contemporary Windsor Chair - <span>$989</span></p>
                <span>Learn more</span>
            </div>
            <div className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://i.pinimg.com/originals/c7/e0/96/c7e096e03783a85a3eb586f6c8b6cfe3.jpg" alt="product image" />
                <p className='fs-4'>Small Classic Dining Set - <span>$185</span></p>
                <span>Learn more</span>
            </div>
        </div>

        <div className="row animate__slow animate__animated animate__bounceInUp px-5 mt-5">
            <div className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://thumbs.dreamstime.com/b/music-gadget-concept-portable-wireless-speakers-phone-headphones-listening-yellow-background-top-view-155271849.jpg" alt="product image" />
                <p className='fs-4'>Classic American Armchair - <span>$2185</span></p>
                <span>Learn more</span>
            </div>
            <div className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://thumbs.dreamstime.com/b/casques-jaunes-%C3%A0-rendu-d-isol%C3%A9s-en-arri%C3%A8re-plan-jaune-220769923.jpg" alt="product image" />
                <p className='fs-4'>Contemporary Furi Writing Desk - <span>$1859</span></p>
                <span>Learn more</span>
            </div>
            <div  className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://techbriefly.com/wp-content/uploads/2021/07/hp4.jpg" alt="product image" />
                <p className='fs-4'>Classic Furi Writing Desk Set - <span>$1859</span></p>
                <span>Learn more</span>
            </div>
            <div className="col-sm-6 col-md-3 mb-3">
                <img className='rounded w-100' src="https://thumbs.dreamstime.com/b/white-earbuds-yellow-pastel-background-white-earbuds-yellow-pastel-background-copy-space-text-149272875.jpg" alt="product image" />
                <p className='fs-4'>Club Deep Seating Settee - <span>$1859</span></p>
                <span>Learn more</span>
            </div>
        </div>

    </div>
  )
}
