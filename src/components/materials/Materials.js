import React from 'react'
import header from "../../img/header.png"
import "./materials.css"
export default function Materials() {
  return (
    <div className='container text-center my-5 '>
        <div className="row gy-3 px-5 justify-content-center news">
            <h2 className='text-center py-5'>Suppliers of High-Quality Materials for Production</h2>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://pbs.twimg.com/profile_images/1272604472238358529/BSwH0ezY_400x400.png" alt="product " />
            </div>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://ecomm.design/wp-content/uploads/2019/08/shopify-payments.png" alt="product image" />
            </div>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://help.shopify.com/assets/manual/shop-pay/shop-pay-logo-do-3.png" alt="product image" />
            </div>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://cdn.shopify.com/shopifycloud/web/assets/v1/25540e5c8ee1c9c066d1263a9e55115a.svg" alt="product image" />
            </div>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://a.fsdn.com/allura/s/shop-pay/icon?1612978104?&w=148" alt="product image" />
            </div>
            <div className="col-sm-3 col-md-2">
                <img className='w-100' src="https://media.sezzle.com/branding/2.0/merchant/howItWorks/sezzle-app-logo.png" alt="product image" />
            </div>
        </div>
    </div>
  )
}
