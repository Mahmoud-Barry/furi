import React from 'react';
import header from "../../img/header.png";
import "./innovations.css";
export default function Innovations() {
  return (
    <div>
        <div className="container text-center py-5">
            <h2>Passion for Innovations</h2>
            <p>The extensive experience of our talented craftsmen helps them create products with more capabilities, functionality, and safety Safety We can ensure our clients that all our furniture products are safe for the health of all their home inhabitants.</p>
            <div className="row my-5 px-5 justify-content-center">
                <div className="col-sm-6 col-md-4">
                    <img className='w-100' src="https://images.theconversation.com/files/394558/original/file-20210412-13-1hd1zpf.jpg?ixlib=rb-1.1.0&rect=0%2C0%2C3600%2C2398&q=45&auto=format&w=926&fit=clip" alt="product image" />
                    <h4  className='py-2'>Safety</h4 >
                    <p className=''>We can ensure our clients that all our furniture products are safe for the health of all their home inhabitants..</p>
                </div>
                <div className="col-sm-6 col-md-4">
                    <img className='w-100' src="https://www.lenouveleconomiste.fr/wp-content/uploads/2020/03/les-10-cles-des-innovations-de-rupture-majeures-qui-nous-attendent-826x459.jpg" alt="product image" />
                    <h4 className='py-2' >Functionality</h4 >
                    <p className=''>Be it a strengthened support structure or hidden support legs for futons, our craftsmen strive to push the boundaries in design functionality..</p>
                </div>
                <div className="col-sm-6 col-md-4">
                    <img className='w-100' src="https://img.freepik.com/photos-gratuite/toucher-monde-virtuel-reseau-connexion_50039-1565.jpg?size=626&ext=jpg" alt="product image" />
                    <h4  className='py-2'>Design</h4 >
                    <p className=''>Our market is overfilled with unique furniture designs. Our goal is to stand out from the crowd and continue to impress our clients..</p>
                </div>
            </div>
        </div>
    </div>
  )
}
